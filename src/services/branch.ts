import type { Branch } from '@/types/branch'
import http from './http'

class BranchService {
  public async save(item: Branch) {
    await http.post('/branch', item)
  }
  public async update(item: Branch) {
    await http.patch('/branch', item)
  }
  public async getAll() {
    const res = await http.get('/branch')
    return res.data
  }
  public async delete(index: number) {
    await http.delete(`/branch/${index}`)
  }
}

export default BranchService
