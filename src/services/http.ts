import { useLoadingStore } from '@/stores/loading'
import axios from 'axios'

const http = axios.create({
  baseURL: 'http://localhost:3000'
})

http.interceptors.response.use(
  async function (res) {
    //fake delay loading
    await new Promise((resolve, reject) => setTimeout(resolve, 500))
    return res
  },
  function (err) {
    useLoadingStore().finishLoad()
    alert(err)
    return Promise.reject(err)
  }
)

export default http
