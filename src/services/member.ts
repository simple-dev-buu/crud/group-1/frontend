import type { Member } from '@/types/member'
import http from './http'

class MemberService {
  public async save(item: Member) {
    await http.post('/member', item)
  }
  public async getAll() {
    const res = await http.get('/member')
    return res.data
  }
  public async delete(index: number) {
    await http.delete(`/member/${index}`)
  }
}

export default MemberService
