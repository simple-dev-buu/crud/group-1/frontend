import type { Product } from '@/types/product'
import http from './http'

class ProductService {
  public async save(item: Product) {
    await http.post('/products', item)
  }
  public async update(item: Product) {
    await http.patch('/products', item)
  }
  public async getAll() {
    const res = await http.get('/products')
    return res.data
  }
  public async delete(index: number) {
    await http.delete(`/products/${index}`)
  }
}

export default ProductService
