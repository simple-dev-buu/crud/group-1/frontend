import type { Promotion } from '@/types/promotion'
import http from './http'

class PromotionService {
  public async save(item: Promotion) {
    await http.post('/promotion', item)
  }
  public async getAll() {
    const res = await http.get('/promotion')
    return res.data
  }
  public async delete(index: number) {
    await http.delete(`/promotion/${index}`)
  }
}

export default PromotionService
