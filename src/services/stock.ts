import type { Stock } from '@/types/stock'
import http from './http'

class StockService {
  public async save(item: Stock) {
    await http.post('/stock', item)
  }
  public async getAll() {
    const res = await http.get('/stock')
    return res.data
  }
  public async delete(index: number) {
    await http.delete(`/stock/${index}`)
  }
}

export default StockService
