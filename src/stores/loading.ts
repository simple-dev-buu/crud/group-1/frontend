import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useLoadingStore = defineStore('loading', () => {
  const loading = ref(false)
  const doLoad = () => {
    loading.value = true
  }
  const finishLoad = () => {
    loading.value = false
  }
  return { loading, doLoad, finishLoad }
})
