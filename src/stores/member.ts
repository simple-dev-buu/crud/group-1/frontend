import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import MemberService from '@/services/member'
import { reactive, ref } from 'vue'
import type { Member } from '@/types/member'

export const useMemberStore = defineStore('member', () => {
  const loadingStore = useLoadingStore()
  const memberService = new MemberService()
  const dialog = ref(false)
  const memberItem = reactive<Member>({
    fName: '',
    lName: '',
    tel: '',
    point: 0,
    birthDate: ''
  })
  const items = ref<Member[]>([])
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'fName', key: 'fame', sortable: false },
    { title: 'lName', key: 'lname', sortable: false },
    { title: 'Tel', key: 'tel', sortable: false },
    { title: 'Point', key: 'point', sortable: false },
    { title: 'BirthDate', key: 'actions', sortable: false },
    { title: 'Actions', key: 'actions', sortable: false }
  ]

  const openDialog = () => {
    dialog.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }
  const save = async () => {
    await memberService.save(memberItem)
    closeDialog()
    getAll()
  }
  const resetItem = () => {
    memberItem.fName = ''
    memberItem.lName = ''
    memberItem.tel = ''
    memberItem.point = 0
    memberItem.birthDate = ''
  }
  const getAll = async () => {
    loadingStore.doLoad()
    items.value = await memberService.getAll()
    loadingStore.finishLoad()
  }
  const editItem = (item: Member) => {
    memberItem.id = item.id
    memberItem.fName = item.fName
    memberItem.lName = item.lName
    memberItem.tel = item.tel
    memberItem.point = item.point
    memberItem.birthDate = item.birthDate
    openDialog()
  }
  const deleteItem = async (item: Member) => {
    if (item.id) {
      await memberService.delete(item.id)
      getAll()
    }
  }

  return {
    headers,
    items,
    memberItem,
    dialog,
    editItem,
    deleteItem,
    openDialog,
    closeDialog,
    save,
    resetItem,
    getAll
  }
})
