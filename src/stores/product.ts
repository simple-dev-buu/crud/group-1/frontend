import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import ProductService from '@/services/product'
import { reactive, ref } from 'vue'
import type { Product } from '@/types/product'

export const useProductStore = defineStore('product', () => {
  const loadingStore = useLoadingStore()
  const productService = new ProductService()
  const dialog = ref(false)
  const productItem = reactive<Product>({
    name: '',
    price: 0,
    type: []
  })
  const items = ref<Product[]>([])
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Price', key: 'price', sortable: false },
    { title: 'Type', key: 'type', sortable: false },
    { title: 'Action', key: 'action' }
  ]

  const openDialog = () => {
    dialog.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }
  const save = async () => {
    if (productItem.id) {
      await productService.update(productItem)
    } else {
      await productService.save(productItem)
    }
    closeDialog()
    getAll()
  }
  const resetItem = () => {
    productItem.id = undefined
    productItem.name = ''
    productItem.price = 0
    productItem.type = []
  }
  const getAll = async () => {
    loadingStore.doLoad()
    items.value = await productService.getAll()
    loadingStore.finishLoad()
  }
  const editItem = (item: Product) => {
    productItem.id = item.id
    productItem.name = item.name
    productItem.price = item.price
    productItem.type = item.type
    console.log(productItem.type)
    openDialog()
  }
  const deleteItem = async (item: Product) => {
    if (item.id) {
      await productService.delete(item.id)
      getAll()
    }
  }

  return {
    headers,
    items,
    productItem,
    dialog,
    editItem,
    deleteItem,
    openDialog,
    closeDialog,
    save,
    resetItem,
    getAll
  }
})
