import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import PromotionService from '@/services/promotion'
import { reactive, ref } from 'vue'
import type { Promotion } from '@/types/promotion'

export const usePromotionStore = defineStore('promotion', () => {
  const loadingStore = useLoadingStore()
  const promotionService = new PromotionService()
  const dialog = ref(false)
  const promotionItem = reactive<Promotion>({
    name: '',
    description: '',
    discount: 0,
    startDate: '',
    endDate: '',
    status: ['active']
  })
  const items = ref<Promotion[]>([])
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Description', key: 'description', sortable: false },
    { title: 'Discount', key: 'discount', sortable: false },
    { title: 'Start Date', key: 'actions', sortable: false },
    { title: 'End Date', key: 'actions', sortable: false },
    { title: 'Status', key: 'actions', sortable: false },
    { title: 'Actions', key: 'actions', sortable: false }
  ]

  const openDialog = () => {
    dialog.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }
  const save = async () => {
    await promotionService.save(promotionItem)
    closeDialog()
    getAll()
  }
  const resetItem = () => {
    promotionItem.name = ''
    promotionItem.description = ''
    promotionItem.discount = 0
    promotionItem.startDate = ''
    promotionItem.endDate = ''
    promotionItem.status = []
  }
  const getAll = async () => {
    loadingStore.doLoad()
    items.value = await promotionService.getAll()
    loadingStore.finishLoad()
  }
  const editItem = (item: Promotion) => {
    promotionItem.id = item.id
    promotionItem.name = item.name
    promotionItem.description = item.description
    promotionItem.discount = item.discount
    promotionItem.startDate = item.startDate
    promotionItem.endDate = item.endDate
    promotionItem.status = item.status
    console.log(promotionItem.status)
    openDialog()
  }
  const deleteItem = async (item: Promotion) => {
    if (item.id) {
      await promotionService.delete(item.id)
      getAll()
    }
  }

  return {
    headers,
    items,
    promotionItem,
    dialog,
    editItem,
    deleteItem,
    openDialog,
    closeDialog,
    save,
    resetItem,
    getAll
  }
})
