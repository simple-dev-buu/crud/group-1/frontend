import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import StockService from '@/services/stock'
import { reactive, ref } from 'vue'
import type { Stock } from '@/types/stock'

export const useStockStore = defineStore('stock', () => {
  const loadingStore = useLoadingStore()
  const stockService = new StockService()
  const dialog = ref(false)
  const dialogS = ref(false)
  const stockItem = reactive<Stock>({
    code: '',
    name: '',
    price: 0,
    balance: 0,
    status: ['ปกติ']
  })
  const items = ref<Stock[]>([])
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Code', key: 'code', sortable: false },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Price', key: 'price', sortable: false },
    { title: 'Balance', key: 'balance', sortable: false },
    { title: 'Status', key: 'actions', sortable: false },
    { title: 'Actions', key: 'actions', sortable: false }
  ]

  const openDialog = () => {
    dialog.value = true
  }
  
  const openDialogStore = () => {
    dialogS.value = true
  }
  

  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }
  const save = async () => {
    await stockService.save(stockItem)
    closeDialog()
    getAll()
  }
  const resetItem = () => {
    stockItem.code = ''
    stockItem.name = ''
    stockItem.price = 0
    stockItem.balance = 0
    stockItem.status = []
  }
  const getAll = async () => {
    loadingStore.doLoad()
    items.value = await stockService.getAll()
    loadingStore.finishLoad()
  }
  const editItem = (item: Stock) => {
    stockItem.id = item.id
    stockItem.code = item.code
    stockItem.name = item.name
    stockItem.price = item.price
    stockItem.balance = item.balance
    stockItem.status = item.status
    console.log(stockItem.status)
    openDialog()
  }
  const deleteItem = async (item: Stock) => {
    if (item.id) {
      await stockService.delete(item.id)
      getAll()
    }
  }

  return {
    headers,
    items,
    stockItem,
    dialog,
    dialogS,
    editItem,
    deleteItem,
    openDialog,
    openDialogStore,
    closeDialog,
    save,
    resetItem,
    getAll
  }
})
