type productType = 'Coffee' | 'Bakery' | 'food'
type Product = {
  id?: number
  name: string
  price: number
  type: productType[]
}

export type { Product, productType }
