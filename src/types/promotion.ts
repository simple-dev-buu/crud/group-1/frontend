type Status = 'active' | 'inactive'
type Promotion = {
  id?: number
  name: string
  description: string
  discount: number
  startDate: string
  endDate: string
  status: Status[]
}

export type { Promotion, Status }
