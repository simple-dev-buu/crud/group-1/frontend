type Status = 'ปกติ' | 'เหลือน้อย'
type Stock = {
  id?: number
  code: string
  name: string
  price: number
  balance: number
  status: Status[]
}

export type { Stock, Status }
